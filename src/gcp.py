import argparse
from resources import googledrive as gdrive


def parse_arguments():
    """Parses the arguments passed by command line.
    
    Returns
    -------
    args : argparse.ArgumentParser
        The object with all parsed arguments
    """
    # Main parser
    parser = argparse.ArgumentParser(description='Script to use resources in Google Cloud Platform (GCP).',
                                     prog='python src\\gcp.py',
                                     usage='%(prog)s <operation> [options]')
    
    subparsers = parser.add_subparsers(title='operations',
                                       help='Select a operation.')
    subparsers.required = True
    subparsers.dest = 'operation'

    # CREATE
    create_msg = 'Creates resources in Google Drive.'
    create_parser = subparsers.add_parser('create',
                                          description=create_msg,
                                          prog='python src\\gcp.py create',
                                          usage='%(prog)s <resource_type> <reource_name> [options]',
                                          help=create_msg)
    create_parser.set_defaults(func=gdrive.create_resource)
    create_parser.add_argument('resource_type',
                               action='store',
                               choices=['folder', 'spreadsheet'],
                               help='Choice the type of resource to create.')
    create_parser.add_argument('resource_name',
                               action='store',
                               help='The name of resource to create.')
    create_parser.add_argument('-p',
                               dest='parent_id',
                               action='store',
                               help='Create the resource inside a folder. Use the ID to select one.')
    # LIST
    list_msg = 'Lists the resources on Google Drive.'
    list_parser = subparsers.add_parser('list',
                                        description=list_msg,
                                        prog='python src\\gcp.py list [options]',
                                        usage='%(prog)s [options]',
                                        help=list_msg)
    list_parser.set_defaults(func=gdrive.list_resources)
    list_parser.add_argument('-f',
                             dest='filter_folder',
                             action='store_true',
                             help='Shows folders')
    list_parser.add_argument('-s',
                             dest='filter_spreadsheet',
                             action='store_true',
                             help='Shows spreadsheets')
    list_parser.add_argument('-p',
                             dest='parent_id',
                             action='store',
                             help='Shows only resources of a specific folder. ' \
                                  'Use the ID attribute to select a folder.')
    # GET
    get_msg = 'Gets a resource on Google Drive.'
    get_parser = subparsers.add_parser('get',
                                       description=get_msg,
                                       prog='python src\\gcp.py get',
                                       usage='%(prog)s <resource_id>',
                                       help=get_msg)
    get_parser.set_defaults(func=gdrive.get_resource)
    get_parser.add_argument('resource_id',
                            action='store', 
                            help='The ID of the resource to be retrieved.')
    
    # DELETE
    delete_msg = 'Deletes a resource in Google Drive.'
    delete_parser = subparsers.add_parser('delete',
                                          description=delete_msg,
                                          prog='python src\\gcp.py delete',
                                          usage='%(prog)s <resource_id>',
                                          help=delete_msg)
    delete_parser.set_defaults(func=gdrive.delete_resource)
    delete_parser.add_argument('resource_id',
                               action='store',
                               help='The ID of the resource to be deleted.')

    # SHARE
    share_msg = 'Shares a resource on Google Drive.'
    share_parser = subparsers.add_parser('share',
                                         description=share_msg,
                                         prog='python src\\gcp.py share',
                                         usage='%(prog)s <role> <email> <resource_id> [options]',
                                         help=share_msg)
    share_parser.set_defaults(func=gdrive.share_resource)
    share_parser.add_argument('role',
                              action='store',
                              choices=['reader', 'writer'],
                              help='Choice the role of the sharing.')
    share_parser.add_argument('email',
                              action='store',
                              help='The email address of sharing.')
    share_parser.add_argument('resource_id',
                              action='store',
                              help='The ID of the resource to be shared.')

    args = parser.parse_args()

    return args


def main():

    options = parse_arguments()
    print('\ngcp.py parameters:')
    for opt in vars(options):
        print(' - %s: %s' % (opt, getattr(options, opt)))
    
    credential = 'credentials/amazonbot.json'
    
    options.func(credential, options)

if __name__ == "__main__":
    main()
