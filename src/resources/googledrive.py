import json
from prettytable import PrettyTable
from googleapiclient import discovery
from googleapiclient.errors import HttpError
from oauth2client.service_account import ServiceAccountCredentials


def mimeType_filter(_type):
    """Assembles the string to filter files by mimeType.
    
    Parameters
    ----------
    _type : str
        The value of mimeType to build the filter.

    Returns
    -------
    filter : str
        The string constains the filter for mimeType attribute.
    """
    return 'mimeType="application/vnd.google-apps.%s"' % _type

def parent_filter(_id):
    """Assembles the string to filter files by parent ID.
    
    Parameters
    ----------
    _id : str
        The value of ID to build the filter.

    Returns
    -------
    filter : str
        The string constains the filter for ID attribute.
    """
    return '"%s" in parents' % _id

def get_service(credential_file):
    """Builds the service object to interact with Google Drive resources.
    The scope is limited to Google Drive API.
    
    Parameters
    ----------
    credential_file : str
        The path of a JSON file with the Google Cloud credentials.

    Returns
    -------
    service : Resource object
        A Resource object with methods for interacting with the service.
    """
    scope = ['https://www.googleapis.com/auth/drive']
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        credential_file, scope
    )
    return discovery.build('drive',
                           'v3',
                           credentials=credentials,
                           cache_discovery=False)

def list_resources(credential_file, options):
    """Lists the resources in Google Drive.
    
    Parameters
    ----------
    credential_file : str
        The path of a JSON file with the Google Cloud credentials.

    options : Namespace object
        An object to take the options passed by user.
    """
    service = get_service(credential_file)
    query = ''
    # Filtering by type
    if options.filter_folder:
        query = ' or '.join(filter(None, [query, mimeType_filter('folder')]))
    if options.filter_spreadsheet:
        query = ' or '.join(filter(None, [query, mimeType_filter('spreadsheet')]))
    if query:
        query = '(%s)' % query
    # Filtering by parent
    if options.parent_id:
        query = ' and '.join(filter(None, [query, parent_filter(options.parent_id)]))
    
    print(' - query: %s' % query)
    
    try:
        resources = service.files().list(q=query).execute()
        
        t = PrettyTable(['ID', 'NAME', 'TYPE'])
        for r in resources['files']:
            t.add_row([r['id'], r['name'], r['mimeType']])
            
        print(t)
    except HttpError as error:
        error_msg = json.loads(error.content).get('error').get('message')
        print('\n'+error_msg)

def create_resource(credential_file, options):
    """Creates a resource in Google Drive.
    
    Parameters
    ----------
    credential_file : str
        The path of a JSON file with the Google Cloud credentials.

    options : Namespace object
        An object to take the options passed by user.
    """
    service = get_service(credential_file)
    resource_body = {
        'name': options.resource_name,
        'mimeType': 'application/vnd.google-apps.%s' % options.resource_type
    }
    
    if options.parent_id:
        resource_body['parents'] = [options.parent_id]
    
    print(' - attributes: %s' % resource_body)
    try:
        resource = service.files().create(body=resource_body).execute()
        print('\n%s %s created. ID: %s' % (options.resource_name,
                                           options.resource_type,
                                           resource.get('id')))
    except HttpError as error:
        print('\n'+error_msg)
    

def get_resource(credential_file, options):
    """Gets a resource in Google Drive.
    
    Parameters
    ----------
    credential_file : str
        The path of a JSON file with the Google Cloud credentials.

    options : Namespace object
        An object to take the options passed by user.
    """
    service = get_service(credential_file)
    try:
        resource_attributes = service.files().get(fileId=options.resource_id,
                                                  fields='*'
        ).execute()
        attributes = PrettyTable(['ID', 'NAME', 'TYPE', 'CREATED'])
        attributes.add_row([resource_attributes['id'],
                            resource_attributes['name'],
                            resource_attributes['mimeType'],
                            resource_attributes['createdTime']])
        print('\nRESOURCE ATTRIBUTES')
        print(attributes)

        permissions = PrettyTable(['EMAIL ADDRESS', 'DISPLAY NAME', 'ROLE'])
        for p in resource_attributes['permissions']:
            permissions.add_row([p['emailAddress'],
                                    p['displayName'],
                                    p['role']])
        print('RESOURCE PERMISSIONS')
        print(permissions)
        return True

    except HttpError as error:
        error_msg = json.loads(error.content).get('error').get('message')
        print('\n'+error_msg)
        return False

def delete_resource(credential_file, options):
    """Deletes a resource in Google Drive.
    
    Parameters
    ----------
    credential_file : str
        The path of a JSON file with the Google Cloud credentials.

    options : Namespace object
        An object to take the options passed by user.
    """
    service = get_service(credential_file)
    get_resource(credential_file, options)
    yes = ['yes', 'y', 'YES', 'Y']
    choice = input('\nDo you wanna proceed with deletion?')
    
    if choice in yes:
        try:
            service.files().delete(fileId=options.resource_id).execute()
            print('\nFile deleted!')
        except HttpError as error:
            error_msg = json.loads(error.content).get('error').get('message')
            print('\n'+error_msg)        
        
    else:
        print('\nNot deleted!')

def share_resource(credential_file, options):
    """Shares a resource in Google Drive.
    
    Parameters
    ----------
    credential_file : str
        The path of a JSON file with the Google Cloud credentials.

    options : Namespace object
        An object to take the options passed by user.
    """
    service = get_service(credential_file)

    batch = service.new_batch_http_request()
    user_permission = {'type': 'user',
                       'role': options.role,
                       'emailAddress': options.email}
    batch.add(service.permissions().create(fileId=options.resource_id,
                                           body=user_permission,
                                           fields='id',))
    batch.execute()
    print('\n%s has %s permission in "%s".' % (options.email, options.role, options.resource_id))