# Google Cloud Platform

A code to interact with resources in Google Cloud Platform (Google Drive).

### Before use
  - Install [GIT](https://git-scm.com/downloads)
  - Install [Python 3.7.4](https://www.python.org/downloads/release/python-374/). 
    - Do not forget to add python on your PATH.
    - Other versions of Python 3 can be used.
  - Download the code:
    - Open a terminal, select a working directory and type:

        ```
        $ git clone https://gitlab.com/upfreelas/google-cloud-platform.git
        ```

    - A folder called google-cloud-platform will be created with all project files. Enter on the folder:

        ```
        $ cd google-cloud-platform
        ```

    - Install the project dependencies:

        ```
        $ pip install -r requirements.txt
        ```

  - Put the JSON file with Google Cloud Platform credentials in credentials folder. Set it on config.ini file.
  - Hint! Always that you want to use the code, check if this repository did not update:

    ```
    $ cd google-cloud-platform
    $ git pull
    ```

     - GIT will pull the code from here if it detects any changes.
     
### Usage
  - To list all resources on Google Drive, type:

    ```
    $ python src\gcp.py list
    ```

  - To create a folder:

    ```
    $ python src\gcp.py create folder <FOLDER_NAME>
    ```

  - You can use other kinds of operations (create, delete, share, etc). To see the option, type:
    
    ```
    $ python src\gcp.py -h
    ```